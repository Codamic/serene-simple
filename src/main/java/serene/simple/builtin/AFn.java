/**
 * Serene (simple) - A PoC lisp to collect data on Serenes concepts
 * Copyright (C) 2019-2020 Sameer Rahmani <lxsameer@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */
package serene.simple.builtin;

import java.util.Collections;
import java.util.List;

import serene.simple.Node;


public abstract class AFn extends Node {

  private List<Object> args;

  public void setArguments() {
    this.args = Collections.EMPTY_LIST;
  }

  public void setArguments(List<Object> args) {
    this.args = args;
  }

  public List<Object> arguments() {
    return this.args;
  }

  public abstract String fnName();

  @Override
  public String toString() {
    return String.format("BuiltinFn<%s>", this.fnName());
  }
}
